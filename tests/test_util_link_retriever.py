import os
from selenium import webdriver
from util_link_retriever import __version__
from util_link_retriever.main import get_sys_arguments
from util_link_retriever.access_url import open_url, ParsedAction


def test_argparse():
    """Testing argument recovery from argparse"""
    the_link = "https://localhost"
    test_parsed_args = get_sys_arguments(["-u", the_link, "-d"])
    assert test_parsed_args.url[0] == the_link, "URL argparse is not correct."
    assert test_parsed_args.debug == True

    test_parsed_args2 = get_sys_arguments(["-u", the_link])
    assert test_parsed_args2.url[0] == the_link, "URL argparse is not correct."
    assert test_parsed_args2.debug == False


def test_open_url():
    """Testing retrieval of page source from url."""
    the_link = "https://www.google.com/"
    test_parsed_args = get_sys_arguments(["-u", the_link])
    page_soup = open_url(test_parsed_args)
    assert page_soup.find("title").string == "Google"


def test_action_parse():
    """Test how actions are processed."""
    the_link = "https://www.selenium.dev/selenium/web/inputs.html"
    options = webdriver.ChromeOptions()
    options.add_argument("--ignore-certificate-errors")
    options.add_argument("--incognito")
    options.add_argument("--headless=new")
    driver = webdriver.Chrome(options=options)

    driver.get(the_link)
    test_action = ParsedAction("click|NAME|submit_input")
    driver = test_action.execute_action(driver)
    title = driver.title

    driver.quit()

    assert title == "inputs", "Click is not working as expected."


def test_version():
    """Test for testing pytest."""
    assert __version__ >= "0.1.0"
