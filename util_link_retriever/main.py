import argparse
import sys
from typing import List
from util_link_retriever.access_url import open_url


def get_sys_arguments(args: List[str]) -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="Retrieve bulk links.")
    parser.add_argument(
        "-u",
        "--url",
        metavar="URL",
        type=str,
        nargs=1,
        required=True,
        help="URL location where links are to be returned from.",
    )
    parser.add_argument(
        "-d",
        "--debug",
        action="store_true",
        default=False,
        help="Flag for additional debug information.",
    )
    parser.add_argument(
        "-a",
        "--actions",
        dest="actions",
        nargs="*",
        help="Specify actions taken on site between URL load and link retrieval. See README.",
    )
    parser.add_argument(
        "-ac",
        "--action-template",
        type=str,
        required=False,
        dest="action_template",
        help="Specify relative path to action template.",
    )

    return parser.parse_args(args)


def driver() -> None:
    args = get_sys_arguments(sys.argv[1:])
    response_soup = open_url(args)
    return None


if __name__ == "__main__":
    driver()
