from selenium import webdriver
from selenium.webdriver.common.by import By
from argparse import ArgumentParser
from bs4 import BeautifulSoup


ACTION_MAP = {
    "CLASS": "CLASS_NAME",
    "NAME": "NAME",
    "TAG": "TAG_NAME",
    "ID": "ID",
    "CSS": "CSS_SELECTOR",
}


class ParsedAction:

    def __init__(self, coded_action: str) -> webdriver:
        """Constructor parsing."""
        print(coded_action.split("|"))
        (self.action, self.type, self.value) = coded_action.split("|")
        return None

    def execute_action(self, br_driver: webdriver) -> None:
        """Execute the action."""
        type = ACTION_MAP[self.type]

        eval(f"br_driver.find_element(By.{type}, '{self.value}').{self.action}()")

        return br_driver


def open_url(args: ArgumentParser, SOUP_PARSER: str = "html.parser") -> BeautifulSoup:
    """Returns the response after opening the url."""
    driver_options = webdriver.ChromeOptions()
    driver_options.add_argument("--ignore-certificate-errors")
    driver_options.add_argument("--incognito")

    if not args.debug:
        driver_options.add_argument("--headless=new")

    driver = webdriver.Chrome(options=driver_options)
    driver.get(args.url[0])
    page_source = driver.page_source
    if args.debug:
        with open("log_url_source.html", "w") as f:
            f.write(page_source)
    driver.quit()

    return BeautifulSoup(page_source, SOUP_PARSER)
